# By Olly F-G 2016

from util import parse_smiles, get_smiles, does_match
from pathfinding import basic_pathfind, simmilarity, MolNode


print '--------------------------------'
start = parse_smiles('CCCC=C(Cl)C')
end = parse_smiles('BrC(C)CCCC')
print "looking for: %s --> %s"%(get_smiles(start), get_smiles(end))
print basic_pathfind(start, end, lambda x:1 )

# By Olly F-G 2016
# Classes to define molecules and reactions and all that

###############################################
'''A Chemical element, with whatever properties are deemed relevant to chemtaxi'''
class Element:
	def __init__(self, symbol_or_number):
		if isinstance(symbol_or_number, int):
			for e in elemental_data:
				if e[0] == symbol_or_number:
					break
			assert e[0] == symbol_or_number, "Element number %s not found"%symbol_or_number
		else:
			symbol = symbol_or_number.title()
			for e in elemental_data:
				if e[1] == symbol:
					break
			assert e[1] == symbol, "Element %s not found"%symbol
		self.atomic_number = e[0]
		self.symbol = e[1]
		self.name = e[2]
		self.valency = e[3]
		self.atomic_mass = e[4]
		self.monoisotopic_mass = e[5]
	def __repr__(self):
		return self.__str__()
	def __str__(self):
		return self.name

###############################################
'''An atom, which is pretty much an Element with charge and aromaticity'''
class Atom(Element):
	def __init__(self, symbol, charge = 0, aromatic = False):
		Element.__init__(self,symbol.title())
		self.charge=charge
		self.aromatic=aromatic
		self.tags=set()
	def __repr__(self):
		return 'Atom(%s,%d)'%(self.symbol,self.charge)
	def __str__(self):
		# Return a string of the atom, eg. Cl-, O2-, etc.
		charge_str = ''
		if self.charge:
			charge_str = '+' if self.charge > 0 else '-'
			if abs(self.charge) > 1:
				charge_str = str(abs(self.charge)) + charge_str
		return self.symbol + charge_str
	def is_equiv(self, mol):
		if mol.symbol == 'R':
			return mol.is_equiv(self)	# True - unless there are exceptions
		return  self.atomic_number == mol.atomic_number and \
				self.charge == mol.charge and \
				self.aromatic == mol.aromatic and \
				len(self.tags) == len(mol.tags)
	def smiles_str(self):
		# Return how this atom should be shown by SMILES
		# Add the element (lowercase if aromatic)
		symb = self.symbol if not self.aromatic else self.symbol.lower()

		# Add charge
		if self.charge:
			charge_str = '+' if self.charge > 0 else '-'
			if abs(self.charge) > 1:
				charge_str = charge_str + str(abs(self.charge))
			symb+=charge_str

		# Add brackets
		if self.charge or self.symbol not in "BCNOPSFClBrIR":
			symb = '['+symb+']'

		# Add any ring tags
		if self.tags:
			# print "Tags: %s"%self.tags
			tags =	''.join([str(tag) for tag in self.tags if tag <= 9]) + \
					''.join(['%'+str(tag) for tag in self.tags if tag > 9])
			# print "Tags: %s"%tags
			symb+=tags
		return symb
	@classmethod
	def from_smiles(cls,atom_str):
		# print atom_str
		# Create a new Atom from the given SMILES string.
		# Use regex to split the atom into the symbol, charge and tags
		import re
		# Get the symbol
		parts_re = re.compile('\[?([A-Z][a-z]?|[a-z])([\+-]+\d?)*([(?:%?\d*)\d])*\]?')
		parts = parts_re.search(atom_str).groups()
		symbol = parts[0]
		Element(symbol)	# Check valid
		# Get aromaticity
		aromatic = symbol[0].lower() == symbol[0]
		# Get charge
		if parts[1]=='+':
			charge=1
		elif parts[1]=='-':
			charge=-1
		else:
			try:
				charge = int(parts[1] or 0)
			except ValueError:
				# +++ format
				sign = parts[1][0]
				charge = int(sign+str(len([c for c in list(parts[1]) if c==sign])))
		# Get tags
		tag_str = parts[2]
		tags=set()
		if tag_str:
			tags=set()
			if '%' in tag_str:
				ind = tag_str.index('%')
				big_tags = tag_str[ind:]
				tag_str = tag_str[:ind]
				tags = tags | set( [ int(n) for n in big_tags.split('%') if n ] )
			tags = tags | set( int(n) for n in list(tag_str) )

		# Make the Atom
		atom = cls(symbol, charge=charge, aromatic=aromatic)
		atom.tags=tags
		return atom

###############################################
'''An R group. Pretty much a very basic atom that has a label on it.'''
class RGroup:
	def __init__(self, label):
		self.label = label
		self.symbol = 'R'
		self.name = 'R Group'
		self.charge = 0
	def __str__(self):
		return self.symbol+(str(self.label) if self.label else '')
	def __repr__(self):
		return 'RGroup(%s)'%self.label
	def is_equiv(self, atom):
		return True	# Can be extendded if we ever want to add exceptions (no-H, etc.)
	def smiles_str(self):
		return self.__str__()
	@classmethod
	def from_smiles(cls, smiles_str):
		# Smiles should always be Rx, so just strip the R
		label = int( smiles_str.strip()[1:] or 0 )
		return cls( label )

###############################################
'''A molecule. Molecules are a graph of Atoms.
The nodes property is a dict of atom:[atoms_bonded].
In the case of tetravalent molecules, the ordering of the bonded atoms is
important as it defines stereochemistry.
Should NOT contain RGroups!!!'''
class Molecule:
	def __init__(self):
		self.nodes={}
	def __str__(self):
		# Return the molecular formula
		counts = {}
		charge = 0
		for atom in self.nodes:
			if atom.symbol not in counts:
				counts[atom.symbol]=0
			counts[atom.symbol]+=1
			charge+=atom.charge
		mol = ''
		for name in sorted(counts.keys()):
			mol+=name+(str(counts[name]) if counts[name] > 1 else '')
		charge_str=''
		if charge:
			charge_str = '+' if charge > 0 else '-'
			if abs(charge) > 1:
				charge_str = str(abs(charge)) + charge_str
		return mol + charge_str
	def __repr__(self):
		rep = []
		numbers = {}
		for k,v in self.nodes.iteritems():
			if k not in numbers:
				numbers[k] = max(numbers.values() or [0] ) + 1
			for m in v:
				if m not in numbers:
					numbers[m] = max(numbers.values() or [0] ) + 1
			rep.append(str(k)+'{'+str(numbers[k])+'}: ('+', '.join([str(m)+'{'+str(numbers[m])+'}' for m in v])+')')
		return '\n'.join(rep)
	# Put Hydrogens in where they should be
	# Number of hydrogens should be:
	# ( valency - bonds ) + charge
	def normalise_hydrogens(self):
		add_n_to = {}
		for atom,bonds in self.nodes.iteritems():
			if isinstance(self, MoleculeFragment) and atom.symbol=='R':
				continue
			existing_hydrogens = len( [ bond for bond in bonds if bond.symbol=='H' ] )
			other_bonds = len(bonds)-existing_hydrogens
			additional_hydrogens = ((atom.valency - other_bonds) + atom.charge ) - existing_hydrogens
			if additional_hydrogens > 0:
				add_n_to[atom] = additional_hydrogens

		for atom,n in add_n_to.iteritems():
			for i in range(n):
				hy = Atom('H')
				self.nodes[hy] = [atom]
				self.nodes[atom].append(hy)
	# Like above but with R groups
	def add_rs(self):
		add_n_to = {}
		r_count = 1
		for atom,bonds in self.nodes.iteritems():
			if isinstance(self, MoleculeFragment) and atom.symbol=='R':
				continue
			existing_rs = len( [ bond for bond in bonds if bond.symbol=='R' ] )
			r_count+=existing_rs
			other_bonds = len(bonds)-existing_rs
			additional_rs = ((atom.valency - other_bonds) + atom.charge ) - existing_rs
			if additional_rs > 0:
				add_n_to[atom] = additional_rs
		for atom,n in add_n_to.iteritems():
			for i in range(n):
				r = RGroup(r_count)
				r_count+=1
				self.nodes[r] = [atom]
				self.nodes[atom].append(r)


###############################################
'''A convenience class. These are just Molecules, only they are allowed R groups.
Used in Reactions as a sort or search term - they are a pattern that needs matching
against real Molecules. Once that's done we can get the R groups from the real Molecule
and re-attach them to the post-reaction MoleculeFragment.
They also have an R counter which keeps track of the next number for an R group'''
class MoleculeFragment(Molecule):
	r_counter=0

###############################################
'''Reactions consist of:
  -   Reactants - MoleculeFragments that go in
  -   Products - MoleculeFragments that come out
  -   Conditions - A dict of conditions that this reaction is  carried out at
  -   Yeild - float from 0-1 for what the average yeild is for this reaction if known.
  			  the yeild for multiple reactions of the same reactants and conditions should
			  never be more than 1 (but may be less tahn 1 due to anaccounted for reactions
			  and equilibriums).
			  And yes, it's spelt wrong on purpose since yield is a restricted name.
  '''
class Reaction:
	def __init__(self, reacts, prods, conds, yeild):
		self.reactants = reacts
		self.products = prods
		self.conditions = conds
		self.yeild = yeild
	def __str__(self):
		return ' + '.join([str(r) for r in self.reactants]) + \
				' ------' + \
				','.join(['%s:%s'%(k,v) for k,v in self.conditions.items()]) + \
				'------> ' + \
				' + '.join([str(p) for p in self.products]) + \
				'    (%.2f%%)'%(self.yeild*100)


# [Atomic number, symbol, name, valency, atomic mass, monoisotopic mass]
elemental_data = [
[1, 'H', 'Hydrogen', -1, 1.008, 1.00782503207],
[6, 'C', 'Carbon', 4, 12.011, 12],
[8, 'O', 'Oxygen', 2, 15.999, 15.99491461956],
[7, 'N', 'Nitrogen', 3, 14.007, 14.00307400478],
[17, 'Cl', 'Chlorine', 1, 35.45, 34.968852682],
[35, 'Br', 'Bromine', 1, 79.904, 78.918337087],
]

# labels for the bitstring that we use to compare molecules with.
bitstring_labels = ['=','#','+','-','Br','Cl','O','N',
# The following are just combinations of C,N and O
'C', 'CC', 'CCC', 'CCCC', 'CCCN', 'CCCO', 'CCN', 'CCNC',
'CCNN', 'CCNO', 'CCO', 'CCOC', 'CCON', 'CCOO', 'CN', 'CNC', 'CNCC', 'CNCN',
'CNCO', 'CNN', 'CNNC', 'CNNN', 'CNNO', 'CNO', 'CNOC', 'CNON', 'CNOO', 'CO',
'COC', 'COCC', 'COCN', 'COCO', 'CON', 'CONC', 'CONN', 'CONO', 'COO', 'COOC',
'COON', 'COOO', 'N', 'NC', 'NCC', 'NCCC', 'NCCN', 'NCCO', 'NCN', 'NCNC', 'NCNN',
'NCNO', 'NCO', 'NCOC', 'NCON', 'NCOO', 'NN', 'NNC', 'NNCC', 'NNCN', 'NNCO',
'NNN', 'NNNC', 'NNNN', 'NNNO', 'NNO', 'NNOC', 'NNON', 'NNOO', 'NO', 'NOC',
'NOCC', 'NOCN', 'NOCO', 'NON', 'NONC', 'NONN', 'NONO', 'NOO', 'NOOC', 'NOON',
'NOOO', 'O', 'OC', 'OCC', 'OCCC', 'OCCN', 'OCCO', 'OCN', 'OCNC', 'OCNN', 'OCNO',
'OCO', 'OCOC', 'OCON', 'OCOO', 'ON', 'ONC', 'ONCC', 'ONCN', 'ONCO', 'ONN',
'ONNC', 'ONNN', 'ONNO', 'ONO', 'ONOC', 'ONON', 'ONOO', 'OO', 'OOC', 'OOCC',
'OOCN', 'OOCO', 'OON', 'OONC', 'OONN', 'OONO', 'OOO', 'OOOC', 'OOON', 'OOOO']

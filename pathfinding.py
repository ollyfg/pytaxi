 # By Olly 2016.
 # Has functions for pathfinding, using the A* algorithm.

from reactions import get_reactions_with
from util import does_match, make_bitstring, react, get_smiles

 # basic pathfinding from A->C
 # Pass in a start, end and a cost function
 # Returns a list of reactions that we can go through to get from A -> C, or None (if it can't find one)
def basic_pathfind(start, end, calc_cost):
	reachable = []
	visited = []

	# Make the start and end molNodes
	start = MolNode( start, cost = 0 )
	end = MolNode( end )

	reachable.append( start )

	while len(reachable):
		node = choose_next_node( reachable, end )
		# print "Node = %s, end = %s"%(get_smiles(node.mol), get_smiles(end.mol))
		if node == end:
			# print "Made it!"
			sequence = []
			while node != None:
				sequence.append(node);
				node=node.previous;

			return sequence

		reachable.remove( node )
		visited.append( node )

		reactions = get_reactions_with( node.mol )
		adjacent_nodes = []
		for reaction in reactions:
			prods = react( reaction, node.mol )
			adjacent_nodes.append( ( reaction, get_most_simmilar( end, prods ) ) )

		for reaction, adjacent_node in adjacent_nodes:
			if adjacent_node not in reachable and adjacent_node not in visited:
				reachable.append( adjacent_node )
			# If this is a new path, or a shorter path than what we have, keep it
			if node.cost+1 < adjacent_node.cost:
				adjacent_node.previous = node
				adjacent_node.cost = node.cost + calc_cost(reaction)


# A class that stores a Molecule along with pathfinding info
class MolNode:
	def __init__(self,mol,cost=99999):
		self.mol = mol
		self.cost = cost
		self.bitstring = make_bitstring( mol )
		self.previous = None
	def __eq__(self, other):
		return does_match(self.mol, other.mol)

# Return a measure of simmilarity between 0 and 1
# This is done by taking the bitstring of both of the molecules, then calculating the Tanimoto Similarity:
# T = (no. of 1s in AND)/(no. of 1s in OR)
def simmilarity(molNode1, molNode2):
	b1 = molNode1.bitstring
	b2 = molNode2.bitstring
	b_and = 0
	b_or = 0
	for bit1, bit2 in zip( b1, b2 ):
		if bit1 and bit2:
			b_and+=1
		if bit1 or bit2:
			b_or+=1
	return float(b_and)/b_or

# Pick the next node we should go to
def choose_next_node(nodes, end):
	best=None;
	minCost=99999;

	for node in nodes:
		cost_to_node = node.cost
		cost_to_end = simmilarity(node,end)
		if cost_to_node+cost_to_end < minCost:
			best=node
			minCost=cost_to_node+cost_to_end


	return best;

# Return the mol from mols that is most simmilar to the given end mols
def get_most_simmilar( end, mols ):
	highest = 0
	best = None
	for mol in mols:
		noded_mol = MolNode( mol )
		sim = simmilarity( end, noded_mol )
		if sim > highest:
			best = noded_mol
			highest = sim
	return best

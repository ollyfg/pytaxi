# By Olly F-G 2016
# Pretty much a database of reactions only not...
from models import Reaction
from util import parse_smiles

# Returna list of all reactions with a certain reactant
def get_reactions_with(query):
	from util import does_match
	for smile_reacts, smile_prods, cond, y in reaction_base:
		reacts = [ parse_smiles(r) for r in smile_reacts ]
		any_matches = False

		for react in reacts:
			if does_match( react, query ):
				any_matches = True
				break

		if not any_matches:
			continue

		prods = [ parse_smiles(r, fragment=True) for r in smile_prods ]
		yield Reaction( reacts, prods, cond, y )

# A generator that goes through all reactions
def get_all_reactions():
	for smile_reacts, smile_prods, cond, y in reaction_base:
		reacts = [ parse_smiles(r) for r in smile_reacts ]
		prods = [ parse_smiles(r) for r in smile_prods ]
		yield Reaction( reacts, prods, cond, y )


reaction_base = [
( ['C(R1)(R2)=C(R3)R4','HH'], ['HC(R1)(R2)C(R3)(R4)H'], {'Catalyst':'Pt'}, 0.9),
( ['R1Cl','HBr'], ['R1Br','HCl'], {}, 0.9),
( ['H+','O-'], ['OR1'], {}, 0.9),
]

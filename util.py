# By Olly F-G 2016
# Functions that deal with molecules

from models import Atom, Molecule, RGroup, MoleculeFragment

# Parse a SMILES string into a Molecule()!
# If fragment is passed, expect possible R groups and make a MoleculeFragment
# If hydrogens is True, fill any gaps with Hydrogens, if it is False, put in R Groups
def parse_smiles(smiles, fragment=False, hydrogens=True):
	import re
	# This R group token is a subset of the atom token
	r_token = re.compile('R\d{0,2}')
	# This regex matches a single atom in SMILES format
	atom_token = re.compile('\[?((?:[A-Z][a-z]?|[a-z])(?:[\+-]+\d?)*(?:(?:(?:%?\d)*|\d))*)\]?')
	rough_components = atom_token.split(smiles)
	# Make sure that all tokens are split to 1 char bits apart from the atoms
	components = []
	# print rough_components
	for c in rough_components:
		if len(c)==1 or atom_token.match(c):
			components.append(c)
		elif len(c) != 1:
			for ch in c:
				components.append(ch)

	# print components

	any_r = fragment
	atom_stack = []
	nodes = {}
	tags = {}
	bond_order = 1
	first = True

	for c in components:
		if c == '=':
			bond_order = 2
		elif c == '#':
			bond_order = 3
		elif c == '(':
			pass
		elif c == ')':
			atom_stack.pop()
		elif atom_token.match( c ):
			# Atom or R group - Create the object
			r = False
			if r_token.match( c ):
				atom = RGroup.from_smiles( c )
				r = any_r = True
			else:
				atom = Atom.from_smiles( c )
			# Add this atom to the molecule
			nodes[atom] = []

			# Make bonds from this atom to it's parent and vice versa
			if first:
				first = False
			else:
				for i in range(bond_order):
					nodes[ atom ].append( atom_stack[ -1 ] )
					nodes[ atom_stack[ -1 ] ].append( atom )
				bond_order = 1

			# Attach to any tags if the second tag is available,
			# otherwise add to tags
			if not r:
				for tag in atom.tags:
					if tag in tags:
						nodes[ tags[tag] ].append( atom )
						nodes[ atom ].append( tags[tag] )
					else:
						tags[ tag ] = atom

			# Add this atom to the atom stack
			atom_stack.append( atom )

	# print atom_stack
	mol = MoleculeFragment() if any_r else Molecule()
	mol.nodes = nodes
	if hydrogens:
		mol.normalise_hydrogens()
	else:
		mol.add_rs()
	return mol

# Return an arbritary weighting of how big this branch is. Most weighting is on Carbons.
def get_size(mol, node, visited):
	weightings = { 'C':10, 'H':1, 'Cl':5, 'Br':5, 'N':7, 'O':6, 'R':0 }
	mw = weightings[node.symbol]
	if node in visited:
		return
	visited.append(node)
	mw+=weightings[node.symbol]
	for cn in mol.nodes[node]:
		get_size(mol, cn, visited)
	return mw



# Return the smiles representation of a molecule recursivley (bleugh!)
def get_smiles(mol):
	def smiles_repr(mol, visited=None, node=None, parent_node=None, rings=None):

		if visited==None:
			visited = []
		if rings==None:
			rings = {}
		if node==None:
			i=0
			root = sorted(mol.nodes.keys())[i]
			try:
				while root.symbol == 'H':
					i+=1
					root = sorted(mol.nodes.keys())[i]
			except IndexError:
				root = sorted(mol.nodes.keys())[0]
			# print "Starting with %s"%root
			return smiles_repr( mol, visited, root, None, rings )

		visited.append(node)
		node_repr=[node]

		# Ignore Hydrogen
		if node.symbol=='H':
			# print "ignoring H"
			return '', visited, rings

		# Order child nodes from shortest -> longest,
		# hiding Hydrogens and tagging the last node,
		# and removing doubles
		child_nodes = []
		for i,cnode in enumerate(mol.nodes[node]):
			if cnode != parent_node:
				bond_order = 1
				# Get the bond order - ie. how many times this atom shows up in mol.nodes[node][:i]
				for n in mol.nodes[node][:i]:
					if n == cnode:
						bond_order+=1
				# Skip the first of each double bonds
				if cnode in mol.nodes[node][i+1:]:
					continue
				# Skip Hydrogens
				if cnode.symbol=='H':
					continue
				child_nodes.append( [ cnode, True, get_size(mol, cnode, [node]), bond_order ] )
		child_nodes.sort(key = lambda x: x[2])
		if child_nodes:
			child_nodes[-1][1] = False
		just_nodes = [n[0] for n in child_nodes]

		for i,(child_node,branch,w,bond_order) in enumerate(child_nodes):
			# print "visited: %s"%(child_node in visited)
			# print "child_nodes: %s"% mol.nodes[node]
			# print "i: %d; "%(i)
			if child_node in visited:
				if child_node not in just_nodes[:i]:
					if child_node != parent_node:
						# Ring
						if child_node in rings:
							# Add this tag to the Atom
							node.tags.add(rings[child_node])
						else:
							ring_num = max( rings.values() or [0] )+1
							rings[child_node]=ring_num
							rings[node]=ring_num
							node.tags.add(ring_num)
							child_node.tags.add(ring_num)
				continue

			if child_node.symbol=='H':
				continue	# Skip Hydrogens
			# if child_node==parent_node:
			# 	continue	# Skip parents
			if branch:
				# Put in parenthesis
				child_repr, visited, rings = smiles_repr( mol, visited, child_node, node, rings )
				node_repr.append('(')
				if bond_order == 2:
					node_repr.append('=')
				elif bond_order == 3:
					node_repr.append('#')
				node_repr+=child_repr
				node_repr.append(')')
			else:
				# Not a branch
				child_repr, visited, rings = smiles_repr( mol, visited, child_node, node, rings )
				if bond_order == 2:
					node_repr.append('=')
				elif bond_order == 3:
					node_repr.append('#')
				node_repr+=child_repr

		return node_repr, visited, rings

	mol_repr, visited, rings = smiles_repr(mol)
	string = ''
	for item in mol_repr:
		if isinstance(item, str):
			string+=item
		elif isinstance(item, Atom) or isinstance(item, RGroup):
			string+=item.smiles_str()
	return string


# For any two molecules or fragments, walk recursivley along the graph of both,
# until either they diverge (not equal, try another path), or they reach an end,
# (end of molecule or an R group) - equal
# Note, mol1 must have an atom other than an R group!
# map_atoms will return an atom map from mol1 -> mol2 if it's true
def match (mol1, mol2):
	from copy import copy
	from itertools import permutations
	def eq(a1, a2, mol1, mol2, visited, depth=0):
		# print "%sComparing %s and %s:"%(depth*'\t',a1,a2)
		depth+=1
		atom_map = {}
		visited = copy(visited)

		# Chack that they aren't already visited
		if a1 in visited and a2 in visited:
			# print "%sAlready visited both."%('\t'*depth)
			return True, {}
		elif a1 in visited or a2 in visited:
			# print "%sAlready Visited."%('\t'*depth)
			return False, {}
		visited.add(a1)
		visited.add(a2)

		# Check for equivalency
		is_r = False
		if a1.is_equiv(a2):
			atom_map[a1]=a2
			if a1.symbol=='R' or a2.symbol=='R':
				# Check if the R group's children are in visited, if they are,
				# we're done!
				all_done = True
				if a1.symbol == 'R':
					a=a1
					mol=mol1
				else:
					a=a2
					mol=mol2

				for c in mol.nodes[a]:
					if c not in visited:
						all_done = False
						is_r = True
				if all_done:
					# print "%smatching to R group."%('\t'*depth)
					return True, atom_map
		else:
			# print "%sNope."%('\t'*depth)
			return False, {}

		# Check that children are the same lengths
		if len(mol1.nodes[a1]) != len(mol2.nodes[a2]) and not is_r:
			# print "%sChildren are the wrong length."%('\t'*depth)
			return False, {}

		# Work out any double (or triple?) bonds
		# When this is done we will have two counts which
		# for each atom says how many times it should be removed from visited
		children1 = mol1.nodes[a1]
		children2 = mol2.nodes[a2]
		counts1 = {}
		counts2 = {}
		for c1,c2 in zip(children1, children2):
			if c1 not in visited:
				if c1 in counts1:
					counts1[c1]+=1
				else:
					counts1[c1] = 0
			if c2 not in visited:
				if c2 in counts2:
					counts2[c2]+=1
				else:
					counts2[c2] = 0
		if (len(counts1.keys()) != len(counts2.keys()) or \
			sum(counts1.values()) != sum(counts2.values()) ):
			# print "%sChildren double/triple bonds don't match."%('\t'*depth)
			return False, {}


		c1_perm = children1
		c2_perms = permutations(children2)



		for c2_perm in c2_perms:
			# print "%sc1perm %s"%('\t'*depth,c1_perm)
			# print "%sc2perm %s"%('\t'*depth, c2_perm)

			full_match = True
			big_atom_map = {}
			for c1,c2 in zip(c1_perm, c2_perm):
				m,a = eq( c1, c2, mol1, mol2, visited, depth=depth )
				if not m:
					full_match = False
					break
				else:
					for k,v in a.iteritems():
						big_atom_map[k] = v

			if full_match:
				# All matched, so return the atom map!
				for k,v in big_atom_map.iteritems():
					atom_map[k] = v
				# print "%sChildren match."%('\t'*depth)
				return True, atom_map

		# print "%sChildren don't match."%('\t'*depth)
		if is_r:
			return True, {}
		return False, {}

	i = 0
	a1 = mol1.nodes.keys()[i]
	try:
		while a1.symbol == 'R':
			i+=1
			a1 = mol1.nodes.keys()[i]
	except IndexError:
		print "Only R groups in mol1!"
		return False, {}

	for a2 in mol2.nodes.keys():
		if a1.is_equiv(a2):
			m,amap = eq(a1,a2,mol1,mol2,set())
			if m:
				return True, amap
	return False, {}

def does_match(mol1, mol2):
	return match(mol1, mol2)[0]


# Turn the atom map we get from match() into a dict of Rn:(root,submol)
# where a submol is just a graph of atoms+bonds, and root is the atom that connects
# where the R group is.
def get_r_groups(atom_map, molf, mol):
	# Decide which side of the map has the R groups
	r_side = 0 if atom_map.keys()[0] in molf.nodes else 1
	# Make a map of R_group:[root], and record all the visited atoms (except the roots)
	visited = set()
	r_group_map = {}
	for mapping in atom_map.iteritems():
		if mapping[ r_side ].symbol == 'R':
			r_group_map[ mapping[ r_side ] ] = [ mapping[ ( r_side + 1 ) % 2 ] ]
		else:
			visited.add( mapping[ ( r_side + 1 ) % 2 ] )

	# A function to recursivley copy a submolecule
	def make_copy(node, mol, parent, visited):
		if node in visited:
			return {}
		visited.add( node )

		submol = { node:[] }

		for child in mol.nodes[ node ]:
			if child in visited and child != parent:
				continue
			submol[ node ].append( child )

			for at,v in make_copy( child, mol, node, visited ).items():
				submol[ at ] = v
		return submol

	# For each R group, make a copy of the sub molecule.
	for r,root_list in r_group_map.iteritems():
		root = root_list[0]
		sub_mol = make_copy( root, mol, None, visited )
		r_group_map[ r ].append(sub_mol)
	return r_group_map


# Carry our a reaction on the given reactants.
# Only Reactatns that at MoleculeFragments can be passed, and any that aren't
# will have their R groups made into CH3.
def react(reaction, *passed_reactants ):
	from copy import copy
	r_group_map = {}
	# Start by mapping all R groups to CH3 groups.
	for r in reaction.reactants:
		for n in r.nodes:
			if n.symbol == 'R':
				c = Atom('C')
				h1 = Atom('H')
				h2 = Atom('H')
				h3 = Atom('H')
				r_group_map[ 'R%s'%n.label ] = [ c, { c:[h1, h2, h3], h1:[c], h2:[c], h3:[c] } ]
	# Now map all the R groups that we were given to what they actually are
	for reactant in reaction.reactants:
		if isinstance(reactant, MoleculeFragment):
			for passed_react in passed_reactants:
				m,a = match(reactant, passed_react)
				if m:
					rgs = get_r_groups(a, reactant, passed_react)
					for r_group, submol_bits in rgs.iteritems():
						r_group_map[ 'R%s'%r_group.label ] = submol_bits
	# REACT!
	# Now we put the R groups onto the products.
	prods = []
	for prod in reaction.products:
		new_product = Molecule()
		new_product.nodes = {}
		for node, bonds in prod.nodes.iteritems():
			if node.symbol == 'R':
				key = 'R%s'%node.label
				# node = r_group_map[ key ][ 0 ]
				for atom,bs in r_group_map[ key ][ 1 ].iteritems():
					if atom not in new_product.nodes:
						new_product.nodes[ atom ] = []
					for b in bs:
						new_product.nodes[ atom ].append( b )
				root = r_group_map[ key ][ 0 ]
				for existing_bond in bonds:
					new_product.nodes[ root ].append( existing_bond )
			else:
				new_bonds = copy(bonds)
				for i,bond in enumerate(new_bonds):
					if bond.symbol == 'R':
						key = 'R%s'%bond.label
						new_bonds[ i ] = r_group_map[ key ][0]
				new_product.nodes[ node ] = new_bonds

		prods.append(new_product)

	return prods

# Return a bitstring of this molecule, used for simmilarity testing
def make_bitstring(mol):
	from models import bitstring_labels
	import array
	bitstring = array.array("B")
	for label in bitstring_labels:
		if label == '=':
			bitstring.append( '=' in get_smiles(mol) )
		elif label == '#':
			bitstring.append( '#' in get_smiles(mol) )
		elif label == '+':
			bitstring.append('+' in str(mol) )
		elif label == '-':
			bitstring.append('-' in str(mol) )
		elif label in 'Cl N O Br':
			bitstring.append(label in str(mol) )
		else:
			re = parse_smiles(label, hydrogens=False)
			bitstring.append( does_match( re, mol ) )
	return bitstring
